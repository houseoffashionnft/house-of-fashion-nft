**House of Fashion NFT**

LCamero is a brand that is focused on creativity and expressing oneself. Our products are inspired by people who are not afraid of who they are or what they want to do. Lisa Camero, the founder, is an artist from Miami who uses paintings and digital media to tell the stories of women. LCamero knows what it takes to create a modern, vibrant, and imaginative clothing items and artworks. She has both ready-to-wear and one-of-a-kind clothing items for sale. Plus a variety of artistic pieces in acrylic paintings, NFTs, and limited edition prints.
[House of Fashion NFT](https://houseoffashion.io/) 

---
